(function(app){
    'use strict';
    var configure=['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){
       $stateProvider
       .state('app',{
        cache: true,
        views: {
            '':{
             templateUrl:"app/components/landing/landing.html"  
            },
            
            'header@app': {
            templateUrl: 'app/shared/layout/header.html',
            controller:'headercntrl as $head'
            },
            'footer@app': {
                templateUrl: 'app/shared/layout/footer.html'
               
            }
        }
           
       })
       .state('app.inventory',{
        display_name: 'Components',
        url: '/inventory',
        cache: true,
        views:{
            'pagecontent':{
                templateUrl:'app/components/inventoryManagement/inventoryTable.html',
                controller:'inventorycontrl as $invnt'
            }


        }
    
       })
       .state('app.product',{
           display_name:'Components',
           url:'/product',
           cache:true,
           views:{
               'pagecontent':{
                templateUrl:'app/components/productlist/productDetail.html',
                controller:'productlistcntrl as $prdct'
               }
           }
       
       })
       .state('app.productviews',{
           display_name:'Productdetail',
           url:'/productviews',
           cache:true,
           views:{
                 'pagecontent':{
                     templateUrl:'app/components/productviews/productview.html',
                     controller:'productviewcntrl as $prdctview'
                 }
           }
       }) 
       .state('app.description',{
        display_name:"description",
        url:'/description/:id',
        cache:true,
        views:{
            'pagecontent':{
                templateUrl:'app/components/productdescription/productdescription.html',
                controller: 'productdescriptioncntrl as $prdctdscrip'
            }

        }
       });
     

    }]


    var run = ['$rootScope', '$state', '$urlRouter', '$timeout', function($rootScope, $state, $urlRouter, $timeout) {

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            if (fromState.url == toState.url) {
                event.preventDefault();
            }
        });
    }];
        
    angular.module('app').run(run);
        
    angular.module('app').config(configure);

})(window.app);