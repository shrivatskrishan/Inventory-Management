(function(app){
    
    angular.module('app').service('prdctdescriptsrvc',prdctdescriptsrvc);
    
    prdctdescriptsrvc.$inject=['$http'];
      
    function prdctdescriptsrvc($http) {
        var url1='https://api.myjson.com/bins/bwh4h';
        var url2='https://api.myjson.com/bins/hv1nt';
        this.getproductdescription=function(){
            return $http.get(url1);
        }
        this.getinventorydescription=function(){
            return $http.get(url2);
        }
        this.updateinventorydescription=function(data){
            return $http.put(url2,data);
        }
    }



})(window.app);