(function(angular) {
  angular.module("app").controller('productdescriptioncntrl', productdescriptioncntrl);
  productdescriptioncntrl.$inject = ["$scope", '$stateParams', 'prdctdescriptsrvc','toastr'];

  function productdescriptioncntrl($scope, $stateParams, prdctdescriptsrvc,toastr) {
      var vm = this;
      //var products=[];
      var productid = $stateParams.id;
      vm.productdetail = [];
      var productlist = [];
      vm.inventory = [];
      var a = [];
    
     // var productvalue=[];
      var productvariant = [];
      var selectedVariantCombo='';
      getproductdescription();


      function getproductdescription() {
          prdctdescriptsrvc.getproductdescription().then(function(response) {

              for (var i = 0; i < response.data.length; i++) {
                  if (productid == response.data[i].Id) {
                      vm.product = response.data[i];
                      vm.discount=response.data[i].discount;
                      vm.description=response.data[i].description;
                      vm.name=response.data[i].name;
                  }
              }

              getinventorydescription(); 
          }, function(error) {
              $scope.status = 'Unable to load customer data:' + error.message;
          });

      }



      function getinventorydescription() {
          prdctdescriptsrvc.getinventorydescription().then(function(response) {
              productvariant = response.data;
              _.forEach(response.data, function(value) {
                  if (productid == value.productId) {
                      vm.inventory.push(value);
                      if (value.default == "Y") {
                          vm.defaultPrice = value.price;
                          selectedVariantCombo=value.inventoryName;
                          vm.variantList = value.inventoryName.split(' / ') || [];
                          productlist = vm.variantList;
                          setActive(vm.variantList);
                      }
                  }
              })
          }, function(error) {
              $scope.status = 'Unable to load customer data: ' + error.message;
          });
      };



      function setActive(variantList) {
          _.forEach(vm.product.variant, function(value, key) {
                _.forEach(value.tags, function(value1){
                    value1.active == 'N'
                })
                _.forEach(variantList, function(value1, key1) {
                    var selectedTagIndex = _.findIndex(value.tags, function(o) { return o.text == value1; });
                    if(selectedTagIndex > -1){
                        value.tags[selectedTagIndex] = {
                            text: value.tags[selectedTagIndex].text,
                            active: 'Y'
                        }
                    }
                });
            })
           
        };



      vm.variantpriceChanged = function(tag, index, variantIndex) {
        vm.disable=false;
        _.forEach(vm.product.variant[variantIndex].tags, function(value1){
            value1.active = 'N'
        })
        _.forEach(vm.product.variant, function(value, key) {
            var selectedTagIndex = _.findIndex(value.tags, function(o) { return o.text == tag.text; });
            if(selectedTagIndex > -1){
                value.tags[selectedTagIndex] = {
                    text: value.tags[selectedTagIndex].text,
                    active: 'Y'
                }
            }
        });
        setPrice(tag, variantIndex);
      }

      var variantList = [];
      function setPrice(tag, variantIndex){
            _.forEach(vm.product.variant, function(value, key){
                variantList[key] = _.find(value.tags, function(o){ return o.active == 'Y'}).text;
            })
              selectedVariantCombo = variantList.join(' / ');
            vm.defaultPrice = _.find(vm.inventory, function(o){ return o.inventoryName == selectedVariantCombo }).price;
          
      };

      vm.buynow=function(){
       
          _.forEach(productvariant, function(value){
         
                if(value.inventoryName==selectedVariantCombo){
                    vm.disable=false;
                    if(value.inventory>0){
                        toastr.success('ok','product is sold');   
                 return value.inventory--;
                 
                    }
                    if(value.inventory==null ||value.inventory==0 ){
                        vm.disable=true;
                    }
                    
             
            }
          })
      
          prdctdescriptsrvc.updateinventorydescription(productvariant);


      }
     


  }

})(window.angular);