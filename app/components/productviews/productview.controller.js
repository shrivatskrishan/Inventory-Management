(function(angular) {
  angular.module("app").controller('productviewcntrl', productviewcntrl);
  productviewcntrl.$inject = ["$scope", 'prdctviewserv'];

  function productviewcntrl($scope, prdctviewserv) {
      var vm = this;
      var products = [];
      var datas = [];
      vm.product = [];
      vm.detail = [];


      getproductvalue();

      function getproductvalue() {
          prdctviewserv.getproductvalue().then(function(response) {
              products = response.data;

              getinventory();
          }, function(error) {
              $scope.status = 'Unable to load customer data:' + error.message;
          });

      }



      function getinventory() {
          prdctviewserv.getinventory().then(function(response) {
              vm.inventory = response.data;
              for (var i = 0; i < response.data.length; i++) {
                  if (response.data[i].default == "Y") {
                      datas.push(response.data[i]);
                      vm.detail = datas;
                  }
              }



              for (var i = 0; i < datas.length; i++) {

                  for (var j = 0; j < products.length; j++) {
                      if (datas[i].productId == products[j].Id) {
                          vm.product.push(products[j]);
                      }
                  }
              }

          }, function(error) {
              $scope.status = 'Unable to load customer data: ' + error.message;
          });

      }




  }




})(window.angular);