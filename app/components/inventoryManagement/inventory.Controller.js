(function(angular) {
    'use strict';

    angular.module("app").controller("inventorycontrl", inventorycontrl);
    inventorycontrl.$inject = ['$scope', 'inventserv',  'toastr'];

    function inventorycontrl($scope, inventserv,toastr) {
        var productInventory = [];
        var vm = this;
        vm.inventory = [];
        var prdct = '';

        getProduct();

        function getProduct() {

            inventserv.getProduct().then(function(response) {
                vm.productdetail = response.data;
            }, function(error) {
                vm.status = 'Unable to load customer data: ' + error.message;
            });
        };

        vm.setProduct = function(prdctdId) {
            prdct = prdctdId;
            vm.product = _.find(vm.productdetail, function(o) {
                return o.Id == prdctdId
            });
            vm.inventory = [];

            getInventory(prdctdId);
        };

        function getInventory(prdctdId) {
            inventserv.getInventory().then(function(response) {
                vm.inventories = response.data;


                productInventory = _.filter(response.data, function(o) {
                    return o.productId == prdctdId
                });

                if (productInventory.length == 0) {
                    getVariantCombinations(vm.product);

                } else {
                    vm.inventory = productInventory;

                }
            }, function(error) {
                vm.status = 'Unable to load customer data: ' + error.message;
            });
        };

        function getVariantCombinations(product) {
            var inventoryData = [];
            var allVariantArray = [];
            product.variant.forEach(function(index) {
                if (index.hasOwnProperty('variantname')) {
                    inventoryData.push(index);
                }
            });

            for (var i = 0; i < inventoryData.length; i++) {
                for (var x = i + 1; x < inventoryData.length; x++) {
                    if (inventoryData[x].variantname == inventoryData[i].variantname) {
                        inventoryData.splice(x, 1);
                        --x;
                    }
                }
            }
            for (var a = 0; a < inventoryData.length; a++) {
                if (inventoryData[a].tags.length != 0) {
                    allVariantArray.push(getTagList(inventoryData[a].tags));
                }
            }
            var result = allPossibleCases(allVariantArray);
            _.forEach(result, function(value) {
                var obj = {
                    Id: createGUID(),
                    productId: product.Id,
                    name: product.name,
                    inventoryName: value,
                    price: product.price,
                    inventory: product.inventory,
                    default: product.default || 'N'
                }
                vm.inventory.push(obj);
            })

        };

        vm.setDefault = function(data) {
            _.forEach(vm.inventory, function(value) {
                if (value.Id == data.Id) {
                    value.default = 'Y';
                } else {
                    value.default = 'N';
                }
            })
        }

        function createGUID() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        };

        function getTagList(tags) {
            var array = [];
            _.forEach(tags, function(value) {
                array.push(value.text);
            })
            return _.uniq(array);
        }

        function allPossibleCases(arr) {
            if (arr.length != 0) {
                if (arr.length == 1) {
                    return arr[0];
                } else {
                    var result = [];
                    var allCasesOfRest = allPossibleCases(arr.slice(1));
                    for (var i = 0; i < allCasesOfRest.length; i++) {
                        for (var j = 0; j < arr[0].length; j++) {
                            result.push(arr[0][j] + " / " + allCasesOfRest[i]);
                        }
                    }
                    return result;
                }
            }
        };

        vm.saveInventory = function() {
            // var index = _.findIndex(vm.inventories, function(o){ return o.productId == vm.inventory.productId});
            // vm.inventories[index] = vm.inventory;
            var vt = vm.inventories;

            for (var i = 0; i < vt.length; i++) {
                var v = productInventory.length;
                if (vt[i].productId == prdct) {
                    var index = vm.inventories.indexOf(vt[i]);
                    vm.inventories.splice(index, v);
                }
            }

            vm.inventories = _.concat(vm.inventories, vm.inventory);
            //vm.inventories = [];
             


            inventserv.saveInventory(vm.inventories).then(function(response) {
                // getInventory();
                toastr.success('detail submited', 'ok');

            }, function(error) {
                vm.status = 'Unable to save inventory data: ' + error.message;
            });
        }

        vm.resetInventory=function(){
            vm.data=[];
            inventserv.saveInventory(vm.data).then(function(response) {
                toastr.warning('Your all data is clear', 'Warning');
                // getInventory();
            }, function(error) {
                vm.status = 'Unable to save inventory data: ' + error.message;
            });

        }



    }


})(window.angular);