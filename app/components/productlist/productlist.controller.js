(function (angular) {
  'use strict';

  angular.module("app").controller("productlistcntrl",productlistcntrl);
  productlistcntrl.$inject=["$scope",'prdctserv','toastr'];
  function productlistcntrl($scope,prdctserv,toastr) {
        var vm=this;
        vm.visible=false;
        
        vm.product = {
            variant: [{
                id: createGUID()
            }],
            shiping:{
                method:'',
                price:''

            }
        };
    
        vm.isEdit = false;

        vm.addbox = function(index) {
            vm.product.variant.push({id: createGUID()});
            
            if(vm.product.variant <= index+1){
                vm.product.variant.splice(index+1,0,name);
            }
        };

        getProduct();
    
        function getProduct() {
            prdctserv.getProduct().then(function (response) {
                vm.products = response.data;
                _.first(vm.products).selected = false;
            }, function (error) {
                 $scope.status = 'Unable to load customer data: ' + error.message;
            });
        }

        function createGUID(){
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        };
        
        vm.save = function() {
           /* if(!_.get(vm.product, 'Id')){
                var as = createGUID();
                vm.products.push({
                    "Id":as,
                    "productname":vm.product.name,
                    variant: vm.product.variant
                })
                prdctserv.updateProduct(vm.products);
            }*
            else{
                angular.forEach(vm.products, function(info){
                    if (info.Id == vm.Id) {
                        info = vm.products;
                    } 
                })
                prdctserv.updateProduct(vm.products);
               
            }*/
            if(!vm.isEdit){ 
                vm.products.push({
                    "Id":createGUID(),
                    "name":vm.product.name,
                    variant: vm.product.variant,
                    "discount":vm.product.discount,
                    "sku":vm.product.sku,
                    "description":vm.product.description,
                     shiping:vm.product.shiping,
                    'shipingvalue':vm.product.shipingvalue,
                    'cod':vm.product.cod
                })
               
                prdctserv.updateProduct(vm.products).then(function (response) {
                    vm.product.selected=true;
                   
                    vm.product = {
                        name: '',
                        variant: [{
                        
                        }],
                        shiping:{
        
                        }
                    };
                    vm.visible=false;
                });

                toastr.success('detail save', 'ok');
              
            }
             else{
                var index= _.findIndex(vm.products, function(info) { return info.Id == vm.Id; });
                vm.products[index]=vm.product;
            
            
                prdctserv.updateProduct(vm.products).then(function (response) {
                    vm.product.selected=true;
                    vm.product = {
                        name: '',
                        variant: [{
                        
                        }],
                        shiping:{
        
                        }
                    };
                    vm.visible=false;
                });
                toastr.success('modification successfully', 'ok');
               
                                   
             }
            
        };

        vm.edittext=function(product){
            vm.isEdit = true;
            vm.product = product;
            vm.visible=true;
         
        }

        vm.delete=function(){
            var newdetail=[];
          
            angular.forEach(vm.products, function(info){
                if(!info.selected){
                    newdetail.push(info);
                }
            }); 
            vm.products=newdetail;
            prdctserv.updateProduct(vm.products);
        }
     
        vm.deleteRow = function($event,index){
           
              if($event.which == 1){
              vm.product.variant.splice(index,1);
              }
              }

             vm.showbox=function(value){
             vm.visible=true;
             }


  }

})(window.angular);